package br.ufg.pos.fswm.pba.lab2spring.vendas.service;

import br.ufg.pos.fswm.pba.lab2spring.vendas.model.Venda;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 12/08/17.
 */
public interface VendasServico {

    Venda buscarPorId(Long id);

    List<Venda> todos();

    Venda salvarNova(Venda venda);

    void excluir(Long id);
}
