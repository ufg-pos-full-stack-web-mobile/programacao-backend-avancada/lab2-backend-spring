package br.ufg.pos.fswm.pba.lab2spring.vendas.service;

import br.ufg.pos.fswm.pba.lab2spring.vendas.model.Venda;
import br.ufg.pos.fswm.pba.lab2spring.vendas.repository.VendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 12/08/17.
 */
@Service
public class VendasServicoImpl implements VendasServico {

    @Autowired
    private VendaRepository repository;

    @Override
    public Venda buscarPorId(Long id) {
        return repository.findOne(id);
    }

    @Override
    public List<Venda> todos() {
        return repository.findAll();
    }

    @Override
    public Venda salvarNova(Venda venda) {
        venda = repository.save(venda);
        return venda;
    }

    @Override
    public void excluir(Long id) {
        repository.delete(id);
    }
}
