package br.ufg.pos.fswm.pba.lab2spring.vendas.repository;

import br.ufg.pos.fswm.pba.lab2spring.vendas.model.Venda;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 12/08/17.
 */
public interface VendaRepository extends JpaRepository<Venda, Long> {
}
