package br.ufg.pos.fswm.pba.lab2spring.vendas.controller;

import br.ufg.pos.fswm.pba.lab2spring.vendas.controller.dtos.VendaDTO;
import br.ufg.pos.fswm.pba.lab2spring.vendas.model.Venda;
import br.ufg.pos.fswm.pba.lab2spring.vendas.service.VendasServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 12/08/17.
 */
@RestController
@RequestMapping("/vendas")
public class VendaResource {

    @Autowired
    private VendasServico vendasServico;

    @GetMapping("/{codigo}")
    public ResponseEntity<VendaDTO> buscarPeloCodigo(@PathVariable("codigo") Long codigo) {
        Venda venda = vendasServico.buscarPorId(codigo);
        VendaDTO vendaDTO = VendaDTO.VendaDTOTransformer.createDto(venda);

        return ResponseEntity.ok(vendaDTO);
    }

    @GetMapping
    public ResponseEntity<List<VendaDTO>> buscarTodas() {
        final List<VendaDTO> vendasDTO = new ArrayList<>();
        final List<Venda> vendas = vendasServico.todos();

        vendas.forEach(venda -> vendasDTO.add(VendaDTO.VendaDTOTransformer.createDto(venda)));

        return ResponseEntity.ok(vendasDTO);
    }

    @PostMapping
    public ResponseEntity<VendaDTO> criarNova(@RequestBody VendaDTO vendaDTO, HttpServletResponse response) {
        Venda venda = VendaDTO.VendaDTOTransformer.createEntity(vendaDTO);
        venda = vendasServico.salvarNova(venda);
        vendaDTO = VendaDTO.VendaDTOTransformer.createDto(venda);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}")
                .buildAndExpand(vendaDTO.getId()).toUri();

        response.setHeader("Location", uri.toASCIIString());

        return ResponseEntity.status(HttpStatus.CREATED).body(vendaDTO);
    }

    @DeleteMapping("/{codigo}")
    public ResponseEntity excluir(@PathVariable("codigo") Long id) {
        vendasServico.excluir(id);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
