package br.ufg.pos.fswm.pba.lab2spring.vendas.controller.dtos;

import br.ufg.pos.fswm.pba.lab2spring.vendas.model.Venda; /**
 * @author Bruno Nogueira de Oliveira
 * @date 12/08/17.
 */
public class VendaDTO {

    private Long id;
    private String produto;
    private Integer quantidade;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public String getProduto() {
        return produto;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public static final class VendaDTOTransformer {
        public static VendaDTO createDto(Venda venda) {
            final VendaDTO vendaDTO = new VendaDTO();
            vendaDTO.setId(venda.getId());
            vendaDTO.setProduto(venda.getProduto());
            vendaDTO.setQuantidade(venda.getQuantidade());
            return vendaDTO;
        }

        public static Venda createEntity(VendaDTO vendaDTO) {
            final Venda venda = new Venda();
            venda.setId(vendaDTO.getId());
            venda.setProduto(vendaDTO.getProduto());
            venda.setQuantidade(vendaDTO.getQuantidade());
            return venda;
        }
    }
}
