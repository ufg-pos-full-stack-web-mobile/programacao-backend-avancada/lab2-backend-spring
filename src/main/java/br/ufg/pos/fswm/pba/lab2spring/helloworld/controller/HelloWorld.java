package br.ufg.pos.fswm.pba.lab2spring.helloworld.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 12/08/17.
 */
@RestController
@RequestMapping("/hello-world")
public class HelloWorld {

    @GetMapping
    public ResponseEntity<String> dizerOlaMundo() {
        final String olaMundo = "Olá mundo";

        return ResponseEntity.ok(olaMundo);
    }
}
