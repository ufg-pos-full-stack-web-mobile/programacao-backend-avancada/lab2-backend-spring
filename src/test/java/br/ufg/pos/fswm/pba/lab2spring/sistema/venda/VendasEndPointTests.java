package br.ufg.pos.fswm.pba.lab2spring.sistema.venda;

import br.ufg.pos.fswm.pba.lab2spring.Lab2SpringApplicationTests;
import br.ufg.pos.fswm.pba.lab2spring.vendas.controller.dtos.VendaDTO;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 12/08/17.
 */
public class VendasEndPointTests extends Lab2SpringApplicationTests {

    @Test
    public void deve_buscar_venda_pelo_id() throws Exception {
        final String json = doGet("/vendas/1");

        VendaDTO vendaEsperada = new VendaDTO();
        vendaEsperada.setId(1L);
        vendaEsperada.setProduto("Coca cola");
        vendaEsperada.setQuantidade(2);
        final String jsonEsperado = gson.toJson(vendaEsperada);

        JSONAssert.assertEquals(jsonEsperado, json, false);
    }

    @Test
    public void deve_buscar_todas_as_vendas() throws Exception {
        List<VendaDTO> vendas = new ArrayList<>();
        final VendaDTO venda1 = new VendaDTO();
        venda1.setId(1L);
        venda1.setProduto("Coca cola");
        venda1.setQuantidade(2);
        vendas.add(venda1);

        final VendaDTO venda2 = new VendaDTO();
        venda2.setId(2L);
        venda2.setProduto("Pastel de Carne");
        venda2.setQuantidade(4);
        vendas.add(venda2);

        final VendaDTO venda3 = new VendaDTO();
        venda3.setId(3L);
        venda3.setProduto("Balinhas");
        venda3.setQuantidade(20);
        vendas.add(venda3);

        final String jsonEsperado = gson.toJson(vendas);
        final String jsonRetornado = doGet("/vendas");

        JSONAssert.assertEquals(jsonEsperado, jsonRetornado, false);
    }

    @Test
    public void deve_ser_possivel_salvar_nova_venda() throws Exception {
        final VendaDTO novaVenda = new VendaDTO();
        novaVenda.setProduto("Americano de presunto");
        novaVenda.setQuantidade(2);

        final String json = gson.toJson(novaVenda);

        ResponseEntity response = doPost("/vendas", json);

        final String location = response.getHeaders().getLocation().toASCIIString();
        assertThat(location).isNotBlank();
        assertThat(location).endsWith("/vendas/4");

        final String jsonCriado = response.getBody().toString();

        novaVenda.setId(4L);

        JSONAssert.assertEquals(gson.toJson(novaVenda), jsonCriado, false);

    }

    @Test
    public void deve_ser_possivel_apagar_venda() throws Exception {
        ResponseEntity response = doDelete("/vendas/2");

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }
}
