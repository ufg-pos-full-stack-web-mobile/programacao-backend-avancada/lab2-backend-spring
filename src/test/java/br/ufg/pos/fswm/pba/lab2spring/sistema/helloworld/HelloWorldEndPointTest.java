package br.ufg.pos.fswm.pba.lab2spring.sistema.helloworld;

import br.ufg.pos.fswm.pba.lab2spring.Lab2SpringApplicationTests;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 12/08/17.
 */
public class HelloWorldEndPointTest extends Lab2SpringApplicationTests {

    @Test
    public void deve_executar_um_hello_world() throws Exception {
        String resposta = doGet("/hello-world");

        assertThat(resposta).isEqualTo("Olá mundo");
    }
}
