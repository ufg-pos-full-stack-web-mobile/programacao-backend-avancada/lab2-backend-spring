package br.ufg.pos.fswm.pba.lab2spring;

import com.google.gson.Gson;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@Sql(scripts = "/load-database.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/clean-database.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public abstract class Lab2SpringApplicationTests {

    protected Gson gson = new Gson();

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setUp() throws Exception {
        // Colocando aqui para executar o SQL de inclusão antes do teste
    }

    protected String doGet(String endpoint) throws Exception {
        ResponseEntity<String> response = executarRequestHttp(endpoint, null, HttpMethod.GET);
        return response.getBody();
    }

    protected ResponseEntity<String> doPost(String endpoint, String json) throws Exception {
        return executarRequestHttp(endpoint, json, HttpMethod.POST);
    }

    protected ResponseEntity<?> doDelete(String endpoint) throws Exception {
        return executarRequestHttp(endpoint, null, HttpMethod.DELETE);
    }

    private ResponseEntity<String> executarRequestHttp(String url, String json, HttpMethod method) throws Exception {
        HttpEntity<String> requestEntity = getStringHttpEntity(json);
        return this.restTemplate.exchange(url, method, requestEntity, String.class);
    }

    private HttpEntity<String> getStringHttpEntity(String json) {
        if(json == null) {
            return getStringHttpEntity();
        }
        HttpHeaders headers = getHeaders();
        return new HttpEntity<>(json, headers);
    }

    private HttpEntity<String> getStringHttpEntity() {
        HttpHeaders headers = getHeaders();
        return new HttpEntity<>(headers);
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "*/*");
        return headers;
    }

    @After
    public void tearDown() throws Exception {
        // para executar o SQL depois do teste
    }
}
